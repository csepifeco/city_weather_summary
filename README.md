## Prerequisites:
   1. If you don't already have, install virtualenv: `pip install virtualenv`
   2. Create a virtual environment `virtualenv venv --distribute`
   3. Activate the environment `source venv/bin/activate`
   4. Install required packages `pip install -r requirements.txt`

## Usage:
Once you have finished with the Prerequisites you can start using the function as below:
```python
from weather.api import get_weather_summary

summary = get_weather_summary('London') 
```


###### This library was created using Python 3.4.4 however it should work with the latest release as well.


