import requests
import logging

from collections import Counter

logging.basicConfig(filename='weather/weather.log',level=logging.ERROR)

APP_NAME = 'Open Weather'
OPEN_WEATHER_MAP_URL = 'http://api.openweathermap.org/data/2.5/forecast/daily'
OPEN_WEATHER_MAP_API_KEY = '0e83edfb1541cb66a71db49f12ac7e98'

RAIN = 'Rain'

def get_weather_summary(city):
    """
    Args:
        city - string:
            The city to prepare the summary for, e.g.: London, Bristol

    Returns - dict:
        A summary for the given city will be returned containing:
            - max temperature over the forecast period
            - min:  min temperature over the forecast period
            - most likely weather condition
            - a list of dates where it is forecast to rain

        Note:   Should the API fail or serve an invalid JSON the return value
                will be None.
    """
    
    if not city:
        return None

    summary = {
        'max': 0,
        'min': 0,
        'most_likely_condition': None,
        'rainy_days_forecast': []
    }

    payload = {
        'q': city,
        'units': 'metric',
        'cnt': 14,
        'APPID': OPEN_WEATHER_MAP_API_KEY
    }

    try:
        response = requests.get(OPEN_WEATHER_MAP_URL, params=payload)
        response.raise_for_status()
        data = response.json()
    except requests.exceptions.RequestException as e:
        logging.error('{}: Failed API Call - {}'.format(APP_NAME, e))
        return None
    except ValueError as e:
        logging.error('{}: Failed to Parse JSON - {}'.format(APP_NAME, e))
        return None

    summary['max'] = max(d['temp']['max'] for d in data['list'])
    summary['min'] = min(d['temp']['min'] for d in data['list'])
    summary['most_likely_condition'] = \
        Counter(d['weather'][0]['description'] \
                for d in data['list']).most_common(1)[0][0]
    summary['rainy_days_forecast'] = [
        d['dt'] for d in data['list'] if d['weather'][0]['main'] == RAIN
    ]

    return summary
